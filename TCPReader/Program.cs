﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;


public class SynchronousSocketListener
{

    // Incoming data from the client.  
    public static string data = null;

    public static void StartListening()
    {
        // Data buffer for incoming data.  
        byte[] bytes = new Byte[1024];

        // Establish the local endpoint for the socket.  
        // Dns.GetHostName returns the name of the   
        // host running the application.  
        IPAddress ipAdd = IPAddress.Parse("192.168.0.18");
        //IPAddress ipAdd = IPAddress.Parse("192.168.3.105");

        //IPHostEntry ipHostInfo = Dns.GetHostEntry("192.168.2.102");  
        //IPAddress ipAddress = ipHostInfo.AddressList[0];  
        IPEndPoint localEndPoint = new IPEndPoint(ipAdd, 8002);

        // Create a TCP/IP socket.  
        Socket listener = new Socket(ipAdd.AddressFamily,
            SocketType.Stream, ProtocolType.Tcp);

        // Bind the socket to the local endpoint and   
        // listen for incoming connections.  
        try
        {
            listener.Bind(localEndPoint);
            listener.Listen(10);

            // Start listening for connections.  
            while (true)
            {
                Console.WriteLine("Waiting for a connection...");
                // Program is suspended while waiting for an incoming connection.  
                Socket handler = listener.Accept();
                data = null;

                // An incoming connection needs to be processed.  
                while (true)
                {
                    int bytesRec = handler.Receive(bytes);
                    data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    Console.WriteLine("Text received : {0}", data);
                    if (data.IndexOf("</soapenv:Envelope>") > -1)
                    {
                        break;
                    }
                }
                if (data.IndexOf("</soapenv:Envelope>") != 0)
                {
                    int startSplit = data.IndexOf("/\">") + 3;
                    int endSplit = data.IndexOf("</web:storeTelemetryList>");
                    int take = endSplit - startSplit;
                    var result = data.Substring(startSplit, take);
                    result = result.Replace("\r\n      ", "");

                    var deviceXML = XDocument.Parse("<storeTelemetryList>" + result + "</storeTelemetryList>")
                    .Descendants("storeTelemetryList")
                    .First();

                    StoreTelemetryList storeTelemetryList = Deserialize<StoreTelemetryList>(deviceXML.ToString());

                    for (int i = 0; i < storeTelemetryList.TelemetryWithDetails.Count; i++)
                    {

                        var sidResponse = GetSID();

                        var deviceResponse = GetDeviceDetails(storeTelemetryList.TelemetryWithDetails[i], sidResponse.eid);

                        bool isAdded = PostDeviceData(storeTelemetryList.TelemetryWithDetails[i], deviceResponse);

                        if (isAdded == true)
                        {
                            Console.WriteLine("========= RESULT ==========");
                            Console.WriteLine("New Record Added In Kuwait Tech System :D");
                            Console.WriteLine("===========================");
                        }
                        else
                        {
                            Console.WriteLine("========= RESULT ==========");
                            Console.WriteLine("New Record Not Added In Kuwait Tech System :(");
                            Console.WriteLine("===========================");
                        }
                    }
                }


                byte[] msg = Encoding.ASCII.GetBytes(data);

                handler.Send(msg);
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();
        }
    }

    private static T Deserialize<T>(string data) where T : class, new()
    {
        if (string.IsNullOrEmpty(data))
            return null;

        var ser = new XmlSerializer(typeof(T));

        using (var sr = new StringReader(data))
        {
            return (T)ser.Deserialize(sr);
        }
    }

    public static int Main(String[] args)
    {
        StartListening();
        return 0;
    }

    public static tokenResponse GetSID()
    {
        string url = "https://hst-api.wialon.com/wialon/ajax.html?svc=token/login&params={\"token\":\"42ea9b1e6193908d797c992eaf91eb8567CD354369C50725657DE5CF28109BECEF40E03A\"}";

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        string myResponse = "";
        using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
        {
            myResponse = sr.ReadToEnd();
        }
        return JsonConvert.DeserializeObject<tokenResponse>(myResponse);
    }

    public static DeviceDetails GetDeviceDetails(TelemetryWithDetails device, string sid)
    {
        string url = "https://hst-api.wialon.com/wialon/ajax.html?svc=core/search_item&params={\"id\":" + device.Telemetry.GpsCode + ",\"flags\":9}&sid=" + sid;
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        string myResponse = "";
        using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
        {
            myResponse = sr.ReadToEnd();
        }
        return JsonConvert.DeserializeObject<DeviceDetails>(myResponse);
    }

    public static bool PostDeviceData(TelemetryWithDetails device, DeviceDetails deviceInfo)
    {
        try
        {
            string url = "https://wsms.kockw.com/VehicleFeedRest/DataService.svc/CarMessage?message=";
            string companyKey = "";
            string parameter = "";

            if (deviceInfo.Item.Nm.StartsWith("14"))
            {
                companyKey = "SLB655288";
                parameter = string.Format("[MessageType=Position Update|VehicleID={0}|Registration={1} |DriverID={2} |Longitude={3} |Latitude={4} |Timestamp={5} |Speed={6} ]&CompanyKey={7}",
             deviceInfo.Item.Flds["2"].V, deviceInfo.Item.Nm, deviceInfo.Item.Flds["3"].V, device.Telemetry.CoordX, device.Telemetry.CoordY, device.Telemetry.Date, device.Telemetry.Speed, companyKey);
            }
            else if (deviceInfo.Item.Nm.StartsWith("20") || deviceInfo.Item.Nm.StartsWith("40"))
            {
                companyKey = "BAKERHUGHES450293";
                parameter = string.Format("[MessageType=Position Update|VehicleID={0}|Registration={1} |DriverID={2} |Longitude={3} |Latitude={4} |Timestamp={5} |Speed={6} |SiteName=Artificial Lift]&CompanyKey={7}",
              deviceInfo.Item.Flds["2"].V, deviceInfo.Item.Nm, deviceInfo.Item.Flds["3"].V, device.Telemetry.CoordX, device.Telemetry.CoordY, device.Telemetry.Date, device.Telemetry.Speed, companyKey);
            }
            else
            {
                return false;
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + parameter);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string myResponse = "";
            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
            {
                myResponse = sr.ReadToEnd();
            }
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
}

public partial class DeviceDetails
{
    [JsonProperty("item")]
    public Item Item { get; set; }

    [JsonProperty("flags")]
    public long Flags { get; set; }
}

public partial class Item
{
    [JsonProperty("nm")]
    public string Nm { get; set; }

    [JsonProperty("cls")]
    public long Cls { get; set; }

    [JsonProperty("id")]
    public long Id { get; set; }

    [JsonProperty("mu")]
    public long Mu { get; set; }

    [JsonProperty("flds")]
    public Dictionary<string, Fld> Flds { get; set; }

    [JsonProperty("fldsmax")]
    public long Fldsmax { get; set; }

    [JsonProperty("uacl")]
    public long Uacl { get; set; }
}

public partial class Fld
{
    [JsonProperty("id")]
    public long Id { get; set; }

    [JsonProperty("n")]
    public string N { get; set; }

    [JsonProperty("v")]
    public string V { get; set; }
}

public partial class DeviceDetails
{
    public static DeviceDetails FromJson(string json) => JsonConvert.DeserializeObject<DeviceDetails>(json, Converter.Settings);
}

public static class Serialize
{
    public static string ToJson(this DeviceDetails self) => JsonConvert.SerializeObject(self, Converter.Settings);
}

internal static class Converter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
    };
}

//internal class StoreTelemetryList
//{
//    public List<telemetryWithDetails> telemetryWithDetails { get; set; }
//}

public class telemetryWithDetails
{
    public telemetry telemetry { get; set; }
    public telemetryDetails telemetryDetails { get; set; }
    public telemetryDetails telemetryDetails1 { get; set; }
    public telemetryDetails telemetryDetails2 { get; set; }
    public telemetryDetails telemetryDetails3 { get; set; }
    public telemetryDetails telemetryDetails4 { get; set; }
    public telemetryDetails telemetryDetails5 { get; set; }
}

public class telemetryDetails
{
    public string sensorCode { get; set; }
    public decimal value { get; set; }
}

public class telemetry
{
    public decimal coordX { get; set; }
    public decimal coordY { get; set; }
    public string date { get; set; }
    public int glonass { get; set; }
    public int gpsCode { get; set; }
    public double speed { get; set; }
}

public class tokenResponse
{
    public string eid { get; set; }
}

[XmlRoot(ElementName = "telemetry")]
public class Telemetry
{
    [XmlElement(ElementName = "coordX")]
    public string CoordX { get; set; }
    [XmlElement(ElementName = "coordY")]
    public string CoordY { get; set; }
    [XmlElement(ElementName = "date")]
    public string Date { get; set; }
    [XmlElement(ElementName = "glonass")]
    public string Glonass { get; set; }
    [XmlElement(ElementName = "gpsCode")]
    public string GpsCode { get; set; }
    [XmlElement(ElementName = "speed")]
    public string Speed { get; set; }
}

[XmlRoot(ElementName = "telemetryDetails")]
public class TelemetryDetails
{
    [XmlElement(ElementName = "sensorCode")]
    public string SensorCode { get; set; }
    [XmlElement(ElementName = "value")]
    public string Value { get; set; }
}

[XmlRoot(ElementName = "telemetryWithDetails")]
public class TelemetryWithDetails
{
    [XmlElement(ElementName = "telemetry")]
    public Telemetry Telemetry { get; set; }
    [XmlElement(ElementName = "telemetryDetails")]
    public List<TelemetryDetails> TelemetryDetails { get; set; }
    [XmlAttribute(AttributeName = "xmlns")]
    public string Xmlns { get; set; }
}

[XmlRoot(ElementName = "storeTelemetryList")]
public class StoreTelemetryList
{
    [XmlElement(ElementName = "telemetryWithDetails")]
    public List<TelemetryWithDetails> TelemetryWithDetails { get; set; }
}
